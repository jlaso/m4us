<?php

/**
 * Step 1: Require the Slim PHP 5 Framework
 *
 * If using the default file layout, the `Slim/` directory
 * will already be on your include path. If you move the `Slim/`
 * directory elsewhere, ensure that it is added to your include path
 * or update this file path as needed.
 */
error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT);
//phpinfo(); die();

define('DEBUG',true);

include './i18n/i18n.php';

// __DIR__ solo está predefinida en php 5.3
if (!defined('__DIR__')) define ('__DIR__',dirname(__FILE__));

require './lib/Slim/Slim.php';
// SlimFunctions to extend Slim
require './lib/Slim/Http/Cookie.php';
require './lib/SlimFunctions.php';
// Slim-extra TwigView extended
require './lib/TwigViewSlim.php';
// ORM: idiorm & paris
require './vendor/Idiorm/idiorm.php';
require './vendor/Paris/paris.php';
// SwiftMailer
require './vendor/SwiftMailer/lib/swift_required.php';
// Models
require './app/models/Modelos.php';

if (DEBUG){
    require_once('./vendor/FirePHPCore/FirePHP.class.php');
    ob_start();
}

TwigViewSlim::$twigDirectory = __DIR__ . '/vendor/Twig/lib/Twig/';

ORM::configure('driver_options', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
ORM::configure('mysql:host=localhost;dbname=dbname');
ORM::configure('username', 'root');
ORM::configure('password', '');
// si necesitamos depurar las sentencias SQL que se generan con IDIORM
ORM::configure('logging', DEBUG);

// constantes que definen los permisos de los usuarios
require './lib/permisos.php';

/**
 * Step 2: Instantiate the Slim application
 *
 * Here we instantiate the Slim application with its default settings.
 * However, we could also pass a key-value array of settings.
 * Refer to the online documentation for available settings.
 */
$app = new Slim(array(
	'templates.path' => __DIR__ . '/app/templates',
	'view' => new TwigViewSlim()        
));

$app->config('web_dir'       ,__DIR__           );
$app->config('web_dir_media' ,__DIR__."/media/"   );
//$app->config('web_dir_slides',__DIR__."/slides/");

$app->config('debug', DEBUG);
/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, and `Slim::delete`
 * is an anonymous function. If you are using PHP < 5.3, the
 * second argument should be any variable that returns `true` for
 * `is_callable()`. An example GET route for PHP < 5.3 is:
 *
 * $app = new Slim();
 * $app->get('/hello/:name', 'myFunction');
 * function myFunction($name) { echo "Hello, $name"; }
 *
 * The routes below work with PHP >= 5.3.
 */
$app->notFound('custom_not_found_handler');
function custom_not_found_handler( Exception $e = NULL ){
    $app = Slim::getInstance();
    $app->render('not_found.html.twig');
}

$app->error('custom_error_handler');
function custom_error_handler( Exception $e = NULL ){
    $app = Slim::getInstance();
    $error = array(
                //'error'=>$e->getMessage(),
                //'trace'=>$e->getTrace(),
                'linea'=>$e->getLine(),
                'file' =>$e->getFile(),
                'code' =>$e->getCode(),
                );
    if (DEBUG){
        $error['trace'] = $e->getTrace();
        $error['error'] = $e->getMessage();        
        
        $firephp = FirePHP::getInstance(true);
        $firephp->log(ORM::get_last_query(), 'ORM-last-query');
        $firephp->log(ORM::get_query_log() , 'ORM-query-log');
    }
    $app->render('error.html.twig', $error );
            
}

require './app/routes/home.php';
require './app/routes/usuarios.php';
require './app/routes/admin.php';


$void = @file_get_contents("http://jaitec.16mb.com/computar.php?host=m4us.tk&ip=".$_SERVER['REMOTE_ADDR']);

// This fires the $app instance to FirePHP in FireFox
SlimFunctions::logFirePHP('app', $app);

/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This is responsible for executing
 * the Slim application using the settings and routes defined above.
 */
$app->run();
