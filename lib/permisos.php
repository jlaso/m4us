<?php

// definicion de los permisos de los distintos usuarios
/*
 * 
sistema de permisos por clave binaria

claves   
 *       C=consulta(vista ampliada), 
 *       A=alta, 
 *       M=modificacion, 
 *       B=baja, 
 *       
 */

// relacionados con usuarios
define('PERM_USER_C', 0x01);
define('PERM_USER_A', 0x02);
define('PERM_USER_M', 0x04);
define('PERM_USER_B', 0x08);
define('PERM_USER'  , 0x0F);




