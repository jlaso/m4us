<?php

require_once 'Views/TwigView.php';

class TwigViewSlim extends TwigView{
	
	private function addFunctions($twigEnvironment) {
		$twigEnvironment->addFunction('urlFor', new Twig_Function_Function('Slim::getInstance()->urlFor'));
		$twigEnvironment->addFunction('asset', new Twig_Function_Function('SlimFunctions::asset'));
                $twigEnvironment->addFunction('logInOut', new Twig_Function_Function('SlimFunctions::logInOut'));
                $twigEnvironment->addFunction('isLoged', new Twig_Function_Function('SlimFunctions::isLoged'));
                $twigEnvironment->addFunction('valor', new Twig_Function_Function('SlimFunctions::valor'));
                $twigEnvironment->addFunction('i18n', new Twig_Function_Function('_'));    
                $twigEnvironment->addFunction('permisos', new Twig_Function_Function('SlimFunctions::permisos'));
                $twigEnvironment->addFunction('flash_', new Twig_Function_Function('SlimFunctions::flash'));
                $twigEnvironment->addFunction('hasPermTo', new Twig_Function_Function('SlimFunctions::hasPermTo'));            
	}
	
	public function getEnvironment() {
		$twigEnvironment = parent::getEnvironment();
		$this->addFunctions($twigEnvironment);
		return $twigEnvironment;
	}		
}

?>