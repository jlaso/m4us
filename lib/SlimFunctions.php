<?php


class SlimFunctions {
    /**
     * Test if the FirePHP class is loaded and DEBUG active and send the log
     * required to the FirePHP console in FireFox.<br/>
     * There is a sample to use:<br/>
     * <pre>
     *  $app->get('/', 'home')->name('homepage');
     *  function home() {
     *  &nbsp;&nbsp;$app = Slim::getInstance();
     *  &nbsp;&nbsp;SlimFunctions::logFirePHP('location','You are in home');
     *  &nbsp;&nbsp;$app->render('home/home.html.twig');
     *  }
     * </pre>
     * @param string $name the name of the var
     * @param mixed $mixed the var want to be logged
     */
    public static function logFirePHP($name,$mixed){
        if(defined('DEBUG') && DEBUG && class_exists("FirePHP")){
                $firephp = FirePHP::getInstance(true);
                $firephp->log($mixed, $name);
        }
    }
    /**
     * get/set the flash value inter-requests
     * @param string $key
     * @param type $value
     * @return type
     */
    public static function flash($key,$value=null){
        if($value==null){
            if (isset($_SESSION["flash_{$key}"])){
                $ret = $_SESSION["flash_{$key}"];
                unset($_SESSION["flash_{$key}"]);
                return $ret;
            }else{
                return null;
            }
        }else{
            $_SESSION["flash_{$key}"] = $value;
        }
    }
    public static function valor($p){
        eval('$r = '.$p.';');
        return $r;
    }
    public static function genKey($length = 7, $repeticiones = false){
      $password = "";
      $possible = "0123456789abcdefghijkmnopqrstuvwxyz";

      if (!$repeticiones && ($length>count_chars($possible))) return -1;

      $i = 0; 
      while ($i < $length) { 
        $char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
        if ($repeticiones || !strstr($password, $char)) {
          $password .= $char;
          $i++;
        }
      }
      return $password;
    }
    //
    public static function asset($url) {                      
        return str_replace("/index.php", "", Slim_Http_Uri::getBaseUri(true))."/".$url;
    }

    public static function isLoged(){
        if (isset($_SESSION['user']) && $_SESSION['user']){
            return 1;
        }else{
            return 0;
        }        
    }
    
    public static function logInOut($class="",$title=""){
        $app = Slim::getInstance();
        $html = '<a class="'.$class.'" title="'.$title.'" href="';
        if (isset($_SESSION['user'])){
            $html .= $app->urlFor('logout').'">Logout('.$_SESSION['user'].')</a>';
        }else{
            $html .= $app->urlFor('login').'">Login</a>';
        }
        return $html;
    }
    
    public static function hasPermTo($perm){        
        $p = 0; //die("->".(PERM_USER_V & $_SESSION["permisos"]));
        try{
            if (isset($_SESSION['permisos'])){
                eval ('$p = ((PERM_'.$perm.' & $_SESSION["permisos"]) > 0);');
            }else{
                $p = 0;
            }
            return $p;
        }catch(Exception $e){
            return 0;
        }
    }
    // $label= etiqueta que va a llevar la lista de permisos
    // $tipo = cadena que identifica el permiso actual, como "USER"
    // $lista= lista de permisos a controlar, como "A,B,..."
    // $suyo = permiso actual que tiene el usuario logado 
    public static function permisos($label,$tipo,$lista,$suyo,$id){
        if ((PERM_USER_C & $_SESSION['permisos'])== 0) return ''; //base_convert($_SESSION['permisos'], 10, 2)."<br/>";   // si no tiene permisos para ver usuarios ... devolver nada        
        $p_a = PERM_USER_A & $_SESSION['permisos'];
        $p_m = PERM_USER_M & $_SESSION['permisos'];
        $texto = array ( 'C'=>'Consultar',
                         'M'=>'Modificar',
                         'A'=>'Alta',
                         'B'=>'Borrar',
                        );
        $const = "PERM_".$tipo;
        eval('$base = '.$const.';');
        $html ='<li><label for="user">'.$label.':</label>';
        $permisos = explode(",", $lista);
        foreach ($permisos as $permiso){
            if (isset($texto[$permiso])){
                eval('$valor = '.$const.'_'.$permiso.';');
                $p = $suyo & $valor;
                $html .= '<input type="checkbox" name="PERM_'.$tipo.'_'.$permiso.'" ';
                if ($p) $html .= 'checked="checked" ';
                if (($id >0) && $p_m || (($id == 0) && $p_a)) ;
                else
                    $html .= 'disabled="disabled"';
                $html .= ' value="'.$valor.'" />'.$texto[$permiso].'&nbsp;';
                $html .= '<input type="hidden" name="PERM[]" value="'.$tipo.'_'.$permiso.'"/>';
            }
        }
        $html .='</li>';
        return $html;
    }
}    
    

?>