<?php

$app->get('/', 'home')->name('homepage');
function home() {
    $app = Slim::getInstance();
    
    SlimFunctions::logFirePHP('location','You are in home');
    $app->render('home/home.html.twig');
}

// login/logout 
$app->get('/login(/:callback)','login')->name('login');
function login($callback=""){
    $app = Slim::getInstance();
    return $app->render('login.html.twig',array('callback'=>$callback));
}

$app->get('/logout', 'logout')->name('logout');
function logout(){
    $app = Slim::getInstance();
    unset($_SESSION['user']);
    unset($_SESSION['user_id']);
    unset($_SESSION['user_control']);
    unset($_SESSION['permisos']);
    $app->redirect($app->urlFor('homepage'));
}

$app->post('/login','do_login')->name('do_login');
function do_login(){
    $app = Slim::getInstance();
    $user = trim(strtolower($app->request()->post('user')));
    $pass = trim($app->request()->post('pass'));
    if (!$user && !$pass){
        $app->flash("error", "No se puede dejar el usuario o la contraseña en blanco");
        $app->redirect($app->urlFor('login'));
    } 
    $callback = $app->request()->post('callback');
    $usuario = Model::factory('Usuario')
            ->where_raw("(`email` = ? OR `apodo` = ?)", array($user, $user))
            //->where('email',  strtolower($user))
            ->find_one();
    //print $usuario->apodo."--".ORM::get_last_query(); die();
    if ($usuario instanceof Usuario && $usuario->password == md5($pass)){
    //if ($user === "admin" && $pass==="admin"){
        $_SESSION['user']         = $usuario->email;
        $_SESSION['user_id']      = $usuario->id;
        $_SESSION['user_control'] = $usuario->control;
        $_SESSION['permisos']     = $usuario->permisos;
        $usuario->alive = date("U");
        $usuario->save();
        if ($callback) $app->redirect($app->urlFor($callback));
                else   $app->redirect($app->urlFor('homepage'));
    }else{
        $app->flash("error", "identificación de usuario errónea");
        $app->redirect($app->urlFor('login'));
    }    
}

?>
