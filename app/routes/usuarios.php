<?php

// recuperar contraseña olvidada
$app->get('/recuperar.php','recuperar_pass')->name('recuperar_pass');
function recuperar_pass(){
    $app = Slim::getInstance();
    
    $app->render('admin/usuario/recuperar_pass.html.twig');                    
}

$app->post('/recuperar.php','recuperar_pass_post')->name('recuperar_pass_post');
function recuperar_pass_post(){
    $app = Slim::getInstance();
    $user = trim(strtolower($app->request()->post('user')));    

    $error = "";

    if (!$user) $error .= "No se puede dejar el usuario en blanco<br/>";    
   
    if (!$error){
        $control = md5(time());
        $pass = SlimFunctions::genKey(8);
        $usuario = Model::factory('Usuarios')->where_equal('email',$user)->find_one();
        if ($usuario instanceof Usuarios){
            $usuario->password = md5($pass);
            $usuario->control = $control;
            $usuario->activo = 0;   // se desactiva por seguridad
            $mensaje = "
                    Tu nuevo password es {$pass},
                    necesito que reactives tu cuenta en el 
                    enlace siguiente:
                    <a href='http://".$_SERVER['HTTP_HOST'].$app->urlFor('activar', array(
                                                    "email"  =>$user,
                                                    "control"=>$control,
                                                    )
                                  )."'>Activar aqui</a>
                    
                    muchas gracias y hasta pronto
                    </body></html>";
                $result = smtp_mail($user,'Recuperar password',"<html><body>$mensaje</body></html>",$mensaje);
                if ($result == 0){
                    $usuario->save();
                    $app->redirect($app->urlFor('gracias', array('mensaje'=>'Gracias por utilizar este servicio para recuperar tu contrase&ntilde;a')));
		}else{
                    $error .= "Ha debido de haber alg&uacute;n tipo de error al enviar el correo de registro<br/>
                    Vu&eacute;lvelo a intentar y si el error persiste m&aacute;ndanos un correo a box@jaitec.net indic&aacute;ndonos lo sucedido, gracias.<br/>
                    error devuelto por la funci&oacute;n: $result<br/>";
                }
        }else{    
            $error .= "No existe el usuario ({$user})<br/>";        
        }
    }
    
    if ($error){
       $app->flash('error', $error);
       $app->redirect($app->urlFor('recuperar_pass', array('user'=>$user)));
    }    
}

// registro de usuario - pantalla
$app->get('/registro.php', 'registro')->name('registro');
function registro() {
    $app = Slim::getInstance();
    return $app->render('usuarios/registro.html.twig');
}

// registrar al usuario - post
$app->post('/registro.php', 'registro_post')->name('registro_post');
function registro_post(){
    $app = Slim::getInstance();
    $user = trim(strtolower($app->request()->post('user')));
    $pass = trim($app->request()->post('password'));
    $pass2 = trim($app->request()->post('password2'));    

    $error = "";

    if (!$user) $error .= "No se puede dejar el usuario en blanco<br/>";    
    if ($pass != $pass2) $error .= "No coinciden las contrase&ntilde;as<br/>";
    if (!$pass) $error .= "No se puede dejar en blanco la contrase&ntilde;a<br/>";
    
    if (!$error){
        $control = md5(time());
        $usuario = Model::factory('Usuarios')->where_equal('email',$user)->find_one();
        if ($usuario instanceof Usuarios){
            $error .= "Ya existe el usuario ({$user})<br/>";
        }else{
            $usuario = Model::factory('Usuarios')->create();
            $usuario->email = $user;
            $usuario->password = md5($pass);
            $usuario->control = $control;
            $usuario->activo = 0;
            $usuario->apodo = $app->request()->post('apodo');
            $link = "http://".$_SERVER['HTTP_HOST'].$app->urlFor('activar', array(
                                                    "email"  =>$user,
                                                    "control"=>$control,
                                                    )
                                  );
            $texto = "Gracias por registrarte en mi web. 
                Haz click en el vinculo siguiente
                {$link}
                para confirmar tu direccion de correo
                electronico.
                
                Muchas gracias y hasta pronto";
            $mensaje = "<html charset='utf-8'><body><p>
                    Gracias por registrarte en mi web.</p><p>Necesito
                    que confirmes que la dirección de correo que has indicado es
                    correcta.</p><p>Haz click en el vínculo siguiente:
                    <a href='{$link}'>Activar aqui</a>
                    </p><p>
                    muchas gracias y hasta pronto
                    </p></body></html>";
            try{
                $result = smtp_mail($user,'Registro en mi web',$texto,$mensaje);
                if (!$result) $error="Error al enviar el correo de activación";
            }catch(Exception $e){
                $error = $e->getMessage();
                $app->error($e);
            }
                if ($result == 0){
                    $usuario->save();
                    $app->redirect($app->urlFor('gracias'));
		}else{
                    $error .= "Ha debido de haber alg&uacute;n tipo de error al enviar el correo de registro<br/>
                    Vu&eacute;lvelo a intentar y si el error persiste m&aacute;ndanos un correo a ___________ indic&aacute;ndonos lo sucedido, gracias.<br/>
                    error devuelto por la funci&oacute;n: $result<br/>";
                }
            }
    }
    
    if ($error){
       $app->flash('error', $error);
       $app->redirect($app->urlFor('registro', array('user'=>$user)));
    }
}

$app->get('/activar.php/:email/:control','activar')->name('activar');
function activar($email,$control){
    $app = Slim::getInstance();
    $usuario = Model::factory('Usuarios')
            ->where_equal('email',$email)
            ->where_equal('control',$control)
            ->where_equal('activo','0')
            ->find_one();
    if ($usuario instanceof Usuarios){
        $usuario->activo = 1;
        $usuario->save();
        return $app->render('usuarios/registro_ok.html.twig');
    }else{
        return $app->notFound();
    }
}

$app->get('/gracias.php','gracias')->name('gracias');
function gracias(){
    $app = Slim::getInstance();
    return $app->render('usuarios/gracias.html.twig');
}

//  REST // AJAX
$app->post('/alive.php','alive')->name('alive');
function alive(){  // para marcar un usuario como conectado
    $app = Slim::getInstance();
    $usu = $app->request()->post('user');
    $ctl = $app->request()->post('control');
    
    if ($usu && $ctl){
        $usuario = Model::factory('Usuarios')
                ->where_equal('id',$usu)
                ->where_equal('control',$ctl)
                ->find_one();

        if ($usuario instanceof Usuarios){
            $usuario->alive = date("U");
            $usuario->save();
            print "1";
        }else{
            print "0";
        }
    }else{
        print "0";
    }
}

$app->get('/users_alive.php','users_alive')->name('users_alive');
function users_alive(){
    $app = Slim::getInstance();
    $fecha = date("U")-300;  // 300 segundos = 5 minutos
    
    $usuarios = Model::factory('Usuarios')
            ->where_gte('alive',$fecha)
            ->find_many();
    
    if (count($usuarios)){
        $app->render('usuarios/alives.html.twig', array('usuarios'=>$usuarios));
    }else{
        print ".";
    }
}
?>
