localStorage['serviceURL'] = "http://localhost/agenda/m/services/";
var serviceURL = localStorage['serviceURL'];

var scroll = new iScroll('wrapper', 
                          { 
                            vScrollbar: false, 
                            hScrollbar: false, 
                            hScroll:    false 
                          }
                         );

var employees;

$(window).load(function() {
	setTimeout(getList, 100);
});

$(document).ajaxError(
    function(event, request, settings) {
            $('#busy').hide();
            alert("Error accessing the server");
    }
);

function getList() {
	$('#busy').show();
	$.getJSON(serviceURL + 'getlista.php', function(data) {
		$('#busy').hide();
		$('#lista li').remove();
		lista = data.items;
		$.each(lista, function(index, elemento) {
			$('#lista').append(
                          '<li><a href="details.php?id=' + elemento.id + '">' +
			  //'<img src="pics/' + elemento.id + '" class="list-icon"/>' +
			  '<p class="line1">' + elemento.fecha + '</p>' +
			  '<p class="line2">' + elemento.texto + '</p>' +
			  '<span class="bubble">' + elemento.id + '</span></a></li>'
                        );
		});
		setTimeout(function(){
			scroll.refresh();
		});
	});
}